Feature: Test Automation
  Prueba de automatizacion

  Scenario: Flujo Test Automation
    Given   El usuario ingresa a la pagina Home de Google
    And     El texto de busqueda existe
    When    Ingresa el texto: Hola Mundo
    And     Realiza la busqueda
    And     Cambia a pestaña: Imagenes
    Then    El sistema se debe redirigir a la pestaña Imagenes