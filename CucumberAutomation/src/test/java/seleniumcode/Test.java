package seleniumcode;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class Test {

    private ChromeDriver driver;

    @Given("^El usuario ingresa a la pagina Home de Google$")
    public void el_usuario_ingresa_a_la_pagina_Home_de_Google() throws Throwable {
        System.out.println("Iniciando Paso 1");
        System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://www.google.cl");
        driver.manage().window().maximize();
    }

    @And("^El texto de busqueda existe$")
    public void el_texto_de_busqueda_existe() throws Throwable {
        System.out.println("Iniciando Paso 2");
        if(driver.findElement(By.name("q")).isDisplayed()){
            System.out.println("Validacion Paso 2: El Texto de busqueda existe");
        }else{
            System.out.println("Validacion Paso 2: El Texto de busqueda no existe");
        }
    }

    @When("^Ingresa el texto: Hola Mundo$")
    public void ingresa_el_texto_Hola_Mundo() throws Throwable {
        System.out.println("Iniciando Paso 3");
        driver.findElement(By.name("q")).sendKeys("Hola mundo");
    }


    @And("^Realiza la busqueda$")
    public void realiza_la_busqueda() throws Throwable {
        System.out.println("Iniciando Paso 4");
        driver.findElement(By.name("q")).submit();
    }

    @And("^Cambia a pestaña: Imagenes$")
    public void cambia_a_pestaña_Imagenes() throws Throwable {
        System.out.println("Iniciando Paso 5");
        driver.findElement(By.xpath("/html/body/div[7]/div/div[4]/div/div/div[1]/div/div[1]/div/div[2]/a")).click();
    }

    @Then("^El sistema se debe redirigir a la pestaña Imagenes$")
    public void el_sistema_se_debe_redirigir_a_la_pestaña_Imagenes() throws Throwable {
        System.out.println("Iniciando Paso 6");
        if(driver.findElement(By.xpath("/html/body/c-wiz/div/div[3]/div[2]/div/div[1]/form/div[1]/div[2]/div/div[3]/div[1]/span")).isDisplayed())
        {
            System.out.println("Validacion Paso 6: Actualmente estamos en la pestaña Imagenes");
        }else{
            System.out.println("Validacion Paso 6: No estamos ubicados en la pestaña Imagenes");
        }

        driver.quit();
    }
}
